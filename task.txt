Existing powers of 2
You will be given a variable list of unknown length, containing 32-bits unsigned integers. You are required to give as output, a comma separated list in the increasing order, showing those powers of 2 that appear at least once, in the powers of 2 decomposition of the integers from the list. If no powers of 2 are present, you should give as output "NA", without quotes. For example, if the list is 1, 3, 4, you are expected to give as output 1, 2 and 4. Integers in the list will be entered one per line.

Case 1:
For the input provided as follows:

1
3
4

Output of the program will be:

1, 2, 4

Case 2:
For the input provided as follows:

3
1
2

Output of the program will be:

1, 2
