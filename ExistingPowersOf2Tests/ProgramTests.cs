﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ExistingPowersOf2.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void GetExistingPowersOf2Test_null_null()
        {
            //arrange
            //act
            var result = Program.GetExistingPowersOf2(null);
            //assert
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void GetExistingPowersOf2Test_emptylistOfIntegers_null()
        {
            //arrange
            //act
            var result = Program.GetExistingPowersOf2(new List<int>());
            //assert
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void GetExistingPowersOf2Test_0_null()
        {
            //arrange
            //act
            var result = Program.GetExistingPowersOf2(new List<int>{0, 0, 0});
            //assert
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void GetExistingPowersOf2Test_validData_validResult()
        {
            //arrange
            var list1 = new List<int>{1, 3, 4};
            var list2 = new List<int>{3, 1, 2};
            var list3 = new List<int>{56, 13, 18};
            var list4 = new List<int>{256};
            //act
            var result1 = Program.GetExistingPowersOf2(list1);
            var result2 = Program.GetExistingPowersOf2(list2);
            var result3 = Program.GetExistingPowersOf2(list3);
            var result4 = Program.GetExistingPowersOf2(list4);
            //assert
            Assert.AreEqual(1, result1[0]);
            Assert.AreEqual(2, result1[1]);
            Assert.AreEqual(4, result1[2]);
            Assert.AreEqual(3, result1.Count);
            Assert.AreEqual(1, result2[0]);
            Assert.AreEqual(2, result2[1]);
            Assert.AreEqual(2, result2.Count);
            Assert.AreEqual(16, result3[4]);
            Assert.AreEqual(32, result3[5]);
            Assert.AreEqual(6, result3.Count);
            Assert.AreEqual(256, result4[0]);
            Assert.AreEqual(1, result4.Count);
        }
    }
}