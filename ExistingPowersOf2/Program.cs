﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ExistingPowersOf2
{
    public class Program
    {
        static void Main()
        {
            var listOfIntegers = GetNumbers();
            PrintNumbers(GetExistingPowersOf2(listOfIntegers));
            Console.ReadKey();
        }

        public static List<double> GetExistingPowersOf2(List<int> listOfIntegers)
         {
            if(listOfIntegers != null && listOfIntegers.Count > 0)
            {
                var listOfbinaryValues = new List<double>();
                foreach (var number in listOfIntegers)
                {
                    string binaryValue = Convert.ToString(number, 2);
                    for(var i = 0; i < binaryValue.Length; i++)
                    {
                        if(binaryValue[i] == '1')
                        {
                            listOfbinaryValues.Add(Math.Pow(2, binaryValue.Length - 1 - i));
                        }
                    }
                }
                return (listOfbinaryValues.Count == 0)? null : listOfbinaryValues.Distinct().OrderBy(x => x).ToList();
            }
            return null;
        }

        public static List<int> GetNumbers()
        {
            var listOfIntegers = new List<int>();
            while (true)
            {
                try
                {
                    listOfIntegers.Add(Convert.ToInt32(Console.ReadLine(), new CultureInfo("pl-PL")));
                }
                catch (Exception)
                {
                    return listOfIntegers;
                }
            }
        }
        public static void PrintNumbers(List<double> listOfValues)
        {
            if (listOfValues == null)
            {
                Console.WriteLine("NA");
            }
            else
            {
                foreach (var number in listOfValues)
                {
                    if (listOfValues.LastOrDefault().Equals(number)) Console.Write(number);
                    else Console.Write(number + ", ");
                }
            }
        }
    }
}
